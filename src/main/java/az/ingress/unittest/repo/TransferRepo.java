package az.ingress.unittest.repo;

import az.ingress.unittest.dto.Transfer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransferRepo extends JpaRepository<Transfer, Long> {
}
