package az.ingress.unittest.controller;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import az.ingress.unittest.dto.Transfer;
import az.ingress.unittest.service.TransferService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transfer")
@RequiredArgsConstructor
public class TransferController {

    private final TransferService transferService;

    @GetMapping("/{id}")
    public ResponseEntity<Transfer> getTransfer(@PathVariable Long id) {
        return ResponseEntity.status(OK).body(transferService.getTransfer(id));
    }

    @PostMapping
    public ResponseEntity<Transfer> postTransfer(@RequestBody Transfer transfer) {
        return ResponseEntity.status(CREATED).body(transferService.postTransfer(transfer));
    }

    @PostMapping("/{name}")
    public ResponseEntity<String> producesString(@PathVariable String name) {
        return ResponseEntity.ok(transferService.producesString(name));
    }
}
