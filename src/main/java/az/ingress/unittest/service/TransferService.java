package az.ingress.unittest.service;

import az.ingress.unittest.dto.Transfer;
import az.ingress.unittest.repo.TransferRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransferService {

    @Value("${topic.name}")
    private String topicName;

    private final KafkaTemplate<String, Object> kafkaTemplate;

    private final TransferRepo transferRepo;

    public Transfer getTransfer(Long id) {
        return transferRepo.findById(id).orElseThrow();
    }

    public Transfer postTransfer(Transfer transfer) {
        return transferRepo.save(transfer);
    }

    public String producesString(String name) {
        Transfer transfer = Transfer.builder()
                .name(name)
                .balance(500.0)
                .build();
        try {

            CompletableFuture<SendResult<String, Object>> sendResult =
                    kafkaTemplate.send(topicName, transfer);

            sendResult.whenComplete((result, ex) -> {

                if (ex == null) {
                    log.info("Data successfully sending to kafka");
                } else {
                    log.error("There occur error");
                }
            });

        } catch (Exception e) {
            log.info("Exception is -- {}", e.getMessage());
        }

        return "OK";
    }
}
