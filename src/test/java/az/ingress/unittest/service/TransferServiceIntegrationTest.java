package az.ingress.unittest.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import az.ingress.unittest.dto.Transfer;
import az.ingress.unittest.repo.TransferRepo;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;


@SpringBootTest
@AutoConfigureMockMvc
class TransferServiceIntegrationTest {

    @Autowired
    TransferRepo transferRepo;

    @Container
    static PostgreSQLContainer postgres = new PostgreSQLContainer("postgres")
            .withDatabaseName("transfer")
            .withPassword("password")
            .withUsername("postgres");

    @DynamicPropertySource
    static void properties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.username", postgres::getUsername);
        registry.add("spring.datasource.password", postgres::getPassword);
        registry.add("spring.datasource.url", postgres::getJdbcUrl);
        registry.add("spring.datasource.databasename", postgres::getDatabaseName);
    }

    @BeforeAll
    static void setup() {
        postgres.start();
    }

//    @AfterAll
//    static void end() {
//        postgres.stop();
//    }

    @Test
    void postTransfer() {

        Transfer transfer = Transfer.builder()
                .balance(1500.00)
                .name("Ismet")
                .build();

        Transfer saveTransfer = transferRepo.save(transfer);
        assertEquals(1L, saveTransfer.getId());

    }

    @Test
    void getTransfer() {

        Transfer transfer = Transfer.builder()
                .balance(500.00)
                .name("Ismet")
                .build();

        Transfer saveTransfer = transferRepo.findById(1L).orElseThrow();
        assertEquals(saveTransfer.getBalance(), transfer.getBalance());

    }

}