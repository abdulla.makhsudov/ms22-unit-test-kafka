package az.ingress.unittest.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import az.ingress.unittest.dto.Transfer;
import az.ingress.unittest.service.TransferService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(TransferController.class)
class TransferControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    TransferService transferService;

    @Test
    public void getTest() throws Exception {

        Transfer transfer = Transfer.builder()
                .balance(500.00)
                .name("Ismet")
                .build();

        when(transferService.getTransfer(any())).thenReturn(transfer);
        mockMvc.perform(get("/transfer/1")
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
//                .andExpect(header().exists("token"))
                .andExpect(status().isOk());
    }

    @Test
    public void postTest() throws Exception {

        Transfer transfer = Transfer.builder()
                .balance(700.00)
                .name("Ismet")
                .build();

        when(transferService.postTransfer(any())).thenReturn(transfer);

        mockMvc.perform(post("/transfer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper(new Transfer(2L, "Ismet", 5000.00))))
                .andExpect(jsonPath("$.name").value("Ismet"))
                .andExpect(jsonPath("$.id").value(1L))
//                .andExpect(header().exists("test"))
                .andExpect(status().isCreated());
    }


    public String mapper(Object o) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(o);
    }

}